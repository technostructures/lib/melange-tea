# Melange-TEA

## Description

This is a library that enables The Elm Architecture for Melange. It is a fork of
[rescript-tea](https://github.com/darklang/rescript-tea), itself a fork of
[bucklescript-tea](https://github.com/OvermindDL1/bucklescript-tea).

[The Elm Architecture](https://guide.elm-lang.org/architecture/) is an MVU pattern
for organizing frontend applications and components. Another example of a
TEA-influenced project is React/Redux.

In TEA, each component has a single model. The model is updated by receiving messages
(typically named `msg`) - all relevent browser and user events, including keyboard,
mouse, fetch, clipboard, etc, are converted into messages. An `update` method
receives a model and message, and returns a new model. It can also return commands,
which affect the outside world, for example by making API calls. And, that's it.
That's the whole idea.

The model is used to render HTML via a built-in virtual DOM. The entire application
is just a single component with a single model and `update` function.

## Advantages

- Entirely event driven, this is like React/Redux but type-safe and significantly faster.
- Amazingly fast compile-times, especially with Dune's built-in watcher
- Open license.

You can read more about it [here](http://blog.overminddl1.com/tags/bucklescript-tea/).

## Project design

- Elm API: Following the Elm API as closely as possible. Converting
    code back and forth between Elm and OCaml should be made as easy as
    possible and there exists both a
    [converter](https://github.com/darklang/philip2), as well as
    [documentation](https://github.com/darklang/philip2#how-to-port-your-project)
    for that process.

## Installation

**To start quickly you can use the [starter template](https://github.com/pdelacroix/melange-tea-template).**

Melange-TEA is not yet available on opam repository, so you need to pin the
package to this repository.

- First verify you have `opam` installed, a switch configured.
- Second you have setup your project for Melange. See the test folder or [See this](https://github.com/melange-re/melange-opam-template)
- Then install via opam:

```opam
depends: [
  "melange-tea" {dev}
]
pin-depends: [
  [ "melange-tea.dev" "git+https://gitlab.com/technostructures/lib/melange-tea.git#1.0.0" ]
]
```

```bash
opam install .
opam pin add your-project.dev .
```

## [Documentation](https://technostructures.gitlab.io/lib/melange-tea/melange-tea/)

## Example

Once you have your Melange project set up and the dependencies configured as above, lets make a new TEA module, the Counter, as is traditional in Elm tutorials.
This file will be named `counter.ml` in your `src` directory for this example. 
Code is described via inline comments:

```ocaml
(* Let's create a new type here to be our main message type that is passed
   around *)
type nonrec msg = Increment | Decrement | Reset | Set of int

(* The model for Counter is just an integer *)
type model = int

(* This is optional for such a simple example, but it is good to have an `init`
   function to define your initial model default values *)
let init () = 0

(* This is the central message handler, it takes as arguments a model and msg
   and returns the updated model *)
let update (model : model) = function
  | Increment ->
      model + 1
  | Decrement ->
      model - 1
  | Reset ->
      0
  | Set v ->
      v

(* This is the main callback to generate the virtual-dom. *)
(* This returns a virtual-dom node that becomes the view, only changes from
   call-to-call are set on the real DOM for efficiency, this is also only
   called once per frame even with many messages sent in within that frame,
   otherwise does nothing *)
let view model =
  (* This opens the Elm-style virtual-dom functions and types into the current
     scope *)
  let open Tea.Html in
  let open Tea.Html.Events in
  let open Tea.Html.Attributes in
  (* This is a helper function for the view that returns a button based on some
     arguments *)
  let view_button title msg = button [onClick msg] [text title] in
  div []
    [ span [style "text-weight" "bold"] [text (Belt.Int.toString model)]
    ; br []
    ; view_button "Increment" Increment
    ; br []
    ; view_button "Decrement" Decrement
    ; br []
    ; view_button "Set to 42" (Set 42)
    ; br []
    ; (if model <> 0 then view_button "Reset" Reset else noNode) ]

(* This is the main function, it can be named anything you want but `main` is
   traditional. The Program returned here has a set of callbacks that can
   easily be called from Melange or from javascript for running this main
   attached to an element, or even to pass a message into the event loop.
   To use this from JS, you can do
   `main(document.getElementById("my-element"))` *)
let main = Tea.App.beginnerProgram {model= init (); update; view}
```

For more examples, see the [examples/ directory](examples/)
