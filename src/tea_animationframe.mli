(** Helper functions for recurring subscriptions
    @canonical Tea.AnimationFrame
*)

type nonrec t = {time: Tea_time.t; delta: Tea_time.t}

val every : ?key:string -> (t -> 'msg) -> 'msg Tea_sub.t

val times : ?key:string -> (key:string -> Tea_time.t -> 'msg) -> 'msg Tea_sub.t

val diffs : ?key:string -> (key:string -> Tea_time.t -> 'msg) -> 'msg Tea_sub.t
