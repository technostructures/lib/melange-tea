let cmd promise tagger =
  let open Vdom in
  Tea_cmd.call (fun callbacks ->
      let _ =
        promise
        |> Js.Promise.then_ (fun res ->
               match tagger res with
               | Some msg ->
                   let () = !callbacks.enqueue msg in
                   Js.Promise.resolve ()
               | None ->
                   Js.Promise.resolve () )
      in
      () )

let result promise msg =
  let open Vdom in
  Tea_cmd.call (fun callbacks ->
      let enq result = !callbacks.enqueue (msg result) in
      let _ =
        promise
        |> Js.Promise.then_ (fun res ->
               let resolve = enq (Ok res) in
               Js.Promise.resolve resolve )
        |> Js.Promise.catch (fun err ->
               let reject = enq (Error (Obj.magic err)) in
               Js.Promise.resolve reject )
      in
      () )
