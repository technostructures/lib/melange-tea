(* fix for Webapi.Dom.Node.appendChild not returning the appended child *)
external appendChild : 'a Dom.node_like -> 'a Dom.node_like = "appendChild"
[@@mel.send.pipe: Dom.node]

type nonrec eventCallback = Dom.event -> unit

type nonrec 'msg systemMessage =
  | Render
  | AddRenderMsg of 'msg
  | RemoveRenderMsg of 'msg

type nonrec 'msg applicationCallbacks =
  {enqueue: 'msg -> unit; on: 'msg systemMessage -> unit}

type nonrec 'msg eventHandler =
  | EventHandlerCallback of string * (Dom.event -> 'msg option)
  | EventHandlerMsg of 'msg

type nonrec 'msg mountHandler =
  | MountHandlerCallback of
      (Dom.element option -> 'msg option) * Dom.element option ref
  | MountHandlerMsg of 'msg

type nonrec 'msg eventCache =
  {handler: eventCallback; cb: (Dom.event -> 'msg option) ref}

type nonrec 'msg property =
  | NoProp
  | RawProp of string * string
  | Attribute of string * string * string
  | Data of string * string
  | Event of string * 'msg eventHandler * 'msg eventCache option ref
  | MountEvent of 'msg mountHandler
  | UnmountEvent of 'msg mountHandler
  | Style of (string * string) list

type nonrec 'msg properties = 'msg property list

type 'msg t =
  | CommentNode of string
  | Text of string
  | Node of string * string * string * string * 'msg properties * 'msg t list
  | LazyGen of string * (unit -> 'msg t) * 'msg t ref
  | Tagger of
      ('msg applicationCallbacks ref -> 'msg applicationCallbacks ref) * 'msg t

let (noNode : 'msg t) = (CommentNode "" : 'msg t)

let comment (s : string) : 'msg t = CommentNode s

let text (s : string) : 'msg t = Text s

let fullnode (namespace : string) (tagName : string) (key : string)
    (unique : string) (props : 'msg properties) (vdoms : 'msg t list) : 'msg t =
  Node (namespace, tagName, key, unique, props, vdoms)

let node ?(namespace : string = "") (tagName : string) ?(key : string = "")
    ?(unique : string = "") (props : 'msg properties) (vdoms : 'msg t list) :
    'msg t =
  fullnode namespace tagName key unique props vdoms

let lazyGen (key : string) (fn : unit -> 'msg t) : 'msg t =
  LazyGen (key, fn, ref noNode)

let (noProp : 'msg property) = (NoProp : 'msg property)

let prop (propName : string) (value : string) : 'msg property =
  RawProp (propName, value)

let onCB ~(key : string) (eventName : string) (cb : Dom.event -> 'msg option) :
    'msg property =
  Event (eventName, EventHandlerCallback (key, cb), ref None)

let onMsg (name : string) (msg : 'msg) : 'msg property =
  Event (name, EventHandlerMsg msg, ref None)

let onMountCB (cb : Dom.element option -> 'msg option) : 'msg property =
  MountEvent (MountHandlerCallback (cb, ref None))

let onMountMsg (msg : 'msg) : 'msg property = MountEvent (MountHandlerMsg msg)

let onUnmountCB (cb : Dom.element option -> 'msg option) : 'msg property =
  UnmountEvent (MountHandlerCallback (cb, ref None))

let onUnmountMsg (msg : 'msg) : 'msg property =
  UnmountEvent (MountHandlerMsg msg)

let attribute (namespace : string) (key : string) (value : string) :
    'msg property =
  Attribute (namespace, key, value)

let data (key : string) (value : string) : 'msg property = Data (key, value)

let style (key : string) (value : string) : 'msg property = Style [(key, value)]

let styles s : 'msg property = Style s

let createElementNsOptional namespace tagName =
  let document = Webapi.Dom.document in
  match namespace with
  | "" ->
      Webapi.Dom.Document.createElement tagName document
  | ns ->
      Webapi.Dom.Document.createElementNS ns tagName document

let nodeAt (index : int) (nodes : Dom.nodeList) : Dom.node =
  Webapi.Dom.NodeList.item index nodes |> Belt.Option.getExn

external setItem : Dom.element -> 'key -> 'value -> unit = "" [@@mel.set_index]

external _getItem : Dom.element -> 'key -> 'value = "" [@@mel.get_index]

let delItem (elem : Dom.element) (key : 'key) =
  setItem elem key Js.Undefined.empty

let rec (renderToHtmlString : 'msg t -> string) =
  ( fun x ->
      match x with
      | CommentNode s ->
          "<!-- " ^ s ^ " -->"
      | Text s ->
          s
      | Node (namespace, tagName, _key, _unique, props, vdoms) ->
          let renderProp x =
            match x with
            | NoProp ->
                ""
            | RawProp (k, v) ->
                String.concat "" [" "; k; "=\""; v; "\""]
            | Attribute (_namespace, k, v) ->
                String.concat "" [" "; k; "=\""; v; "\""]
            | Data (k, v) ->
                String.concat "" [" data-"; k; "=\""; v; "\""]
            | Event (_, _, _) ->
                ""
            | MountEvent _ ->
                ""
            | UnmountEvent _ ->
                ""
            | Style s ->
                String.concat ""
                  [ " style=\""
                  ; String.concat ";"
                      (List.map
                         (fun (k, v) -> String.concat "" [k; ":"; v; ";"])
                         s )
                  ; "\"" ]
          in
          String.concat ""
            [ "<"
            ; namespace
            ; (if namespace = "" then "" else ":")
            ; tagName
            ; String.concat "" (List.map (fun p -> renderProp p) props)
            ; ">"
            ; String.concat "" (List.map (fun v -> renderToHtmlString v) vdoms)
            ; "</"
            ; tagName
            ; ">" ]
      | LazyGen (_key, gen, _cache) ->
          let vdom = gen () in
          renderToHtmlString vdom
      | Tagger (_tagger, vdom) ->
          renderToHtmlString vdom
    : 'msg t -> string )

let (emptyEventHandler : eventCallback) = (fun _ev -> () : eventCallback)

let emptyEventCB _ev : eventCallback option = None

let eventHandler (callbacks : 'msg applicationCallbacks ref)
    (cb : (Dom.event -> 'msg option) ref) : eventCallback =
 fun ev -> match !cb ev with None -> () | Some msg -> !callbacks.enqueue msg

let (eventHandlerGetCB : 'msg eventHandler -> Dom.event -> 'msg option) =
  ( fun x ->
      match x with
      | EventHandlerCallback (_, cb) ->
          cb
      | EventHandlerMsg msg ->
          fun _ev -> Some msg
    : 'msg eventHandler -> Dom.event -> 'msg option )

let compareEventHandlerTypes (left : 'msg eventHandler) :
    'msg eventHandler -> bool =
 fun x ->
  match x with
  | EventHandlerCallback (cb, _) -> (
    match left with
    | EventHandlerCallback (lcb, _) when cb = lcb ->
        true
    | _ ->
        false )
  | EventHandlerMsg msg -> (
    match left with EventHandlerMsg lmsg when msg = lmsg -> true | _ -> false )

let eventHandlerRegister (callbacks : 'msg applicationCallbacks ref)
    (elem : Dom.eventTarget) (name : string) (handlerType : 'msg eventHandler) :
    'msg eventCache option =
  let cb = ref (eventHandlerGetCB handlerType) in
  let handler = eventHandler callbacks cb in
  let () = Webapi.Dom.EventTarget.addEventListener name handler elem in
  Some {handler; cb}

let eventHandlerUnregister (elem : Dom.eventTarget) (name : string) :
    'msg eventCache option -> 'msg eventCache option =
 fun x ->
  match x with
  | None ->
      None
  | Some cache ->
      let () =
        Webapi.Dom.EventTarget.removeEventListener name cache.handler elem
      in
      None

let eventHandlerMutate (callbacks : 'msg applicationCallbacks ref)
    (elem : Dom.eventTarget) (oldName : string) (newName : string)
    (oldHandlerType : 'msg eventHandler) (newHandlerType : 'msg eventHandler)
    (oldCache : 'msg eventCache option ref)
    (newCache : 'msg eventCache option ref) : unit =
  match !oldCache with
  | None ->
      newCache := eventHandlerRegister callbacks elem newName newHandlerType
  | Some oldcache ->
      if oldName = newName then
        let () = newCache := !oldCache in
        if compareEventHandlerTypes oldHandlerType newHandlerType then ()
        else
          let cb = eventHandlerGetCB newHandlerType in
          let () = oldcache.cb := cb in
          ()
      else
        let () = oldCache := eventHandlerUnregister elem oldName !oldCache in
        let () =
          newCache := eventHandlerRegister callbacks elem newName newHandlerType
        in
        ()

let mountHandlerCall (callbacks : 'msg applicationCallbacks ref)
    (handlerType : 'msg mountHandler) : unit =
  match handlerType with
  | MountHandlerMsg msg ->
      !callbacks.enqueue msg
  | MountHandlerCallback (cb, elem) -> (
    match cb !elem with None -> () | Some msg -> !callbacks.enqueue msg )

external is_connected : Dom.element -> bool = "isConnected" [@@mel.get]

let rec execMountHandlers (callbacks : 'msg applicationCallbacks ref)
    (elem : Dom.element option) (vNode : 'msg t) : unit =
  let rec exec callbacks elem vNode =
    match vNode with
    | Tagger (tagger, vdom) ->
        exec (tagger callbacks) elem vdom
    | Node (_, _, _, _, properties, children) ->
        Belt.List.forEach children (fun child -> exec callbacks elem child) ;
        Belt.List.forEach properties (function
          | MountEvent handlerType ->
              mountHandlerCall callbacks handlerType
          | _ ->
              () )
    | _ ->
        ()
  in
  match vNode with
  | Tagger (tagger, vdom) ->
      execMountHandlers (tagger callbacks) elem vdom
  | Node (_, _, _, _, properties, _) ->
      Belt.List.forEach properties (function
        | MountEvent (MountHandlerCallback (_, elem_ref)) ->
            elem_ref := elem
        | _ ->
            () ) ;
      Belt.Option.forEach elem (fun e ->
          if is_connected e then exec callbacks elem vNode )
  | _ ->
      ()

let rec execUnmountHandlers (callbacks : 'msg applicationCallbacks ref)
    (elem : Dom.element option) (vNode : 'msg t) : unit =
  match vNode with
  | Tagger (tagger, vdom) ->
      execUnmountHandlers (tagger callbacks) elem vdom
  | Node (_, _, _, _, properties, children) ->
      let () =
        Belt.List.forEach children (fun child ->
            execUnmountHandlers callbacks elem child )
      in
      Belt.List.forEach properties (function
        | UnmountEvent handlerType ->
            mountHandlerCall callbacks handlerType
        | _ ->
            () )
  | _ ->
      ()

let patchVNodesOnElemsPropertiesApplyAdd
    (callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
    (_idx : int) x =
  match x with
  | NoProp ->
      ()
  | RawProp (k, v) ->
      setItem elem k v
  | Attribute (namespace, k, v) ->
      Webapi.Dom.Element.setAttributeNS namespace k v elem
  | Data (k, v) ->
      Js.log ("TODO:  Add Data Unhandled", k, v) ;
      failwith "TODO:  Add Data Unhandled"
  | Event (name, handlerType, cache) ->
      let eventTarget = Webapi.Dom.Element.asEventTarget elem in
      cache := eventHandlerRegister callbacks eventTarget name handlerType
  | MountEvent _ ->
      ()
  | UnmountEvent _ ->
      ()
  | Style s -> (
    match Webapi.Dom.HtmlElement.ofElement elem with
    | Some elem ->
        let elemStyle = Webapi.Dom.HtmlElement.style elem in
        List.fold_left
          (fun () (k, v) ->
            Webapi.Dom.CssStyleDeclaration.setProperty k v "" elemStyle )
          () s
    | None ->
        failwith "Expected htmlelement in patchVNodesOnElems_PropertiesApplyAdd"
    )

let patchVNodesOnElemsPropertiesApplyRemove
    (_callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
    (_idx : int) x =
  match x with
  | NoProp ->
      ()
  | RawProp (k, _v) ->
      delItem elem k
  | Attribute (namespace, k, _v) ->
      Webapi.Dom.Element.removeAttributeNS namespace k elem
  | Data (k, v) ->
      Js.log ("TODO:  Remove Data Unhandled", k, v) ;
      failwith "TODO:  Remove Data Unhandled"
  | Event (name, _, cache) ->
      let eventTarget = Webapi.Dom.Element.asEventTarget elem in
      cache := eventHandlerUnregister eventTarget name !cache
  | MountEvent _ ->
      ()
  | UnmountEvent _ ->
      ()
  | Style s -> (
    match Webapi.Dom.HtmlElement.ofElement elem with
    | Some elem ->
        let elemStyle = Webapi.Dom.HtmlElement.style elem in
        List.fold_left
          (fun () (k, _v) ->
            Webapi.Dom.CssStyleDeclaration.removeProperty k elemStyle |> ignore
            )
          () s
    | None ->
        failwith
          "Expected htmlelement in patchVNodesOnElems_PropertiesApply_Remove" )

let patchVNodesOnElemsPropertiesApplyRemoveAdd
    (callbacks : 'msg applicationCallbacks ref) (elem : Dom.element) (idx : int)
    (oldProp : 'msg property) (newProp : 'msg property) : unit =
  let () = patchVNodesOnElemsPropertiesApplyRemove callbacks elem idx oldProp in
  let () = patchVNodesOnElemsPropertiesApplyAdd callbacks elem idx newProp in
  ()

let patchVNodesOnElemsPropertiesApplyMutate
    (_callbacks : 'msg applicationCallbacks ref) (elem : Dom.element)
    (_idx : int) (oldProp : 'msg property) x =
  match x with
  | NoProp ->
      failwith
        "This should never be called as all entries through NoProp are gated."
  | RawProp (k, v) ->
      setItem elem k v
  | Attribute (namespace, k, v) ->
      Webapi.Dom.Element.setAttributeNS namespace k v elem
  | Data (k, v) ->
      Js.log ("TODO:  Mutate Data Unhandled", k, v) ;
      failwith "TODO:  Mutate Data Unhandled"
  | Event (_newName, _newHandlerType, _newCache) ->
      failwith "This will never be called because it is gated"
  | MountEvent _ ->
      failwith "This will never be called because it is gated"
  | UnmountEvent _ ->
      failwith "This will never be called because it is gated"
  | Style s -> (
    match Webapi.Dom.HtmlElement.ofElement elem with
    | None ->
        failwith
          "Expected htmlelement in patchVNodesOnElemsPropertiesApplyMutate"
    | Some elem -> (
        let elemStyle = Webapi.Dom.HtmlElement.style elem in
        match oldProp with
        | Style oldS ->
            List.fold_left2
              (fun () (ok, ov) (nk, nv) ->
                if ok = nk then
                  if ov = nv then ()
                  else
                    Webapi.Dom.CssStyleDeclaration.setProperty nk nv ""
                      elemStyle
                else
                  let (_ : string) =
                    Webapi.Dom.CssStyleDeclaration.removeProperty ok elemStyle
                  in
                  Webapi.Dom.CssStyleDeclaration.setProperty nk nv "" elemStyle
                )
              () oldS s
        | _ ->
            failwith
              "Passed a non-Style to a new Style as a Mutations while the old \
               Style is not actually a style!" ) )

let rec patchVNodesOnElemsPropertiesApply
    (callbacks : 'msg applicationCallbacks ref) (elem : Dom.element) (idx : int)
    (oldProperties : 'msg property list) (newProperties : 'msg property list) :
    bool =
  match[@ocaml.warning "-4"] (oldProperties, newProperties) with
  | [], [] ->
      true
  | [], _newProp :: _newRest ->
      false
  | _oldProp :: _oldRest, [] ->
      false
  | NoProp :: oldRest, NoProp :: newRest ->
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | ( (RawProp (oldK, oldV) as oldProp) :: oldRest
    , (RawProp (newK, newV) as newProp) :: newRest ) ->
      let () =
        if oldK = newK && oldV = newV then ()
        else
          patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx oldProp
            newProp
      in
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | ( (Attribute (oldNS, oldK, oldV) as oldProp) :: oldRest
    , (Attribute (newNS, newK, newV) as newProp) :: newRest ) ->
      let () =
        if oldNS = newNS && oldK = newK && oldV = newV then ()
        else
          patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx oldProp
            newProp
      in
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | ( (Data (oldK, oldV) as oldProp) :: oldRest
    , (Data (newK, newV) as newProp) :: newRest ) ->
      let () =
        if oldK = newK && oldV = newV then ()
        else
          patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx oldProp
            newProp
      in
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | ( (Event (oldName, oldHandlerType, oldCache) as _oldProp) :: oldRest
    , (Event (newName, newHandlerType, newCache) as _newProp) :: newRest ) ->
      let eventTarget = Webapi.Dom.Element.asEventTarget elem in
      let () =
        eventHandlerMutate callbacks eventTarget oldName newName oldHandlerType
          newHandlerType oldCache newCache
      in
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | MountEvent _ :: oldRest, MountEvent _ :: newRest ->
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | UnmountEvent _ :: oldRest, UnmountEvent _ :: newRest ->
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | (Style oldS as oldProp) :: oldRest, (Style newS as newProp) :: newRest ->
      let () =
        if oldS = newS then ()
        else
          patchVNodesOnElemsPropertiesApplyMutate callbacks elem idx oldProp
            newProp
      in
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest
  | oldProp :: oldRest, newProp :: newRest ->
      let () =
        patchVNodesOnElemsPropertiesApplyRemoveAdd callbacks elem idx oldProp
          newProp
      in
      patchVNodesOnElemsPropertiesApply callbacks elem (idx + 1) oldRest newRest

let patchVNodesOnElemsProperties (callbacks : 'msg applicationCallbacks ref)
    (elem : Dom.element) (oldProperties : 'msg property list)
    (newProperties : 'msg property list) : bool =
  patchVNodesOnElemsPropertiesApply callbacks elem 0 oldProperties newProperties

let genEmptyProps (length : int) : 'msg property list =
  let rec aux lst x =
    match x with 0 -> lst | len -> aux (noProp :: lst) (len - 1)
  in
  aux [] length

let mapEmptyProps (props : 'msg property list) : 'msg property list =
  List.map (fun _ -> noProp) props

let rec patchVNodesOnElemsReplaceNode
    (oldCallbacks : 'msg applicationCallbacks ref)
    (callbacks : 'msg applicationCallbacks ref) (elem : Dom.node)
    (elems : Dom.nodeList) (idx : int) : 'msg t -> unit =
 fun x ->
  match x with
  | Node
      (newNamespace, newTagName, _newKey, _newUnique, newProperties, newChildren)
    as vNode ->
      let oldChild = nodeAt idx elems in
      let newChild = createElementNsOptional newNamespace newTagName in
      let (_ : bool) =
        patchVNodesOnElemsProperties callbacks newChild
          (mapEmptyProps newProperties)
          newProperties
      in
      let newChildNode = Webapi.Dom.Element.asNode newChild in
      let childChildren = Webapi.Dom.Node.childNodes newChildNode in
      let () =
        patchVNodesOnElems oldCallbacks callbacks newChildNode childChildren 0
          [] newChildren
      in
      let attachedChild =
        Webapi.Dom.Node.insertBefore newChildNode oldChild elem
      in
      let () =
        execMountHandlers callbacks
          (Webapi.Dom.Element.ofNode attachedChild)
          vNode
      in
      let _removedChild = Webapi.Dom.Node.removeChild oldChild elem in
      ()
  | _ ->
      failwith
        "Node replacement should never be passed anything but a node itself"

and patchVNodesOnElemsCreateElement
    (oldCallbacks : 'msg applicationCallbacks ref)
    (callbacks : 'msg applicationCallbacks ref) : 'msg t -> Dom.node =
 fun x ->
  match x with
  | CommentNode s ->
      Webapi.Dom.Document.createComment s Webapi.Dom.document
      |> Webapi.Dom.Comment.asNode
  | Text text ->
      Webapi.Dom.Document.createTextNode text Webapi.Dom.document
      |> Webapi.Dom.Text.asNode
  | Node (newNamespace, newTagName, _newKey, _unique, newProperties, newChildren)
    ->
      let newChild = createElementNsOptional newNamespace newTagName in
      let true =
        patchVNodesOnElemsProperties callbacks newChild
          (mapEmptyProps newProperties)
          newProperties
          [@@ocaml.warning "-8"]
      in
      let newChildNode = Webapi.Dom.Element.asNode newChild in
      let childChildren = Webapi.Dom.Node.childNodes newChildNode in
      let () =
        patchVNodesOnElems oldCallbacks callbacks newChildNode childChildren 0
          [] newChildren
      in
      (* let () = execMountHandlers callbacks (Some newChild) vNode in *)
      newChildNode
  | LazyGen (_newKey, newGen, newCache) ->
      let vdom = newGen () in
      let () = newCache := vdom in
      patchVNodesOnElemsCreateElement oldCallbacks callbacks vdom
  | Tagger (tagger, vdom) ->
      patchVNodesOnElemsCreateElement oldCallbacks (tagger callbacks) vdom

and patchVNodesOnElemsMutateNode (oldCallbacks : 'msg applicationCallbacks ref)
    (callbacks : 'msg applicationCallbacks ref) (elem : Dom.node)
    (elems : Dom.nodeList) (idx : int) (oldNode : 'msg t) (newNode : 'msg t) :
    unit =
  match (oldNode, newNode) with
  | ( ( Node
          ( _oldNamespace
          , oldTagName
          , _oldKey
          , oldUnique
          , oldProperties
          , oldChildren ) as oldNode )
    , ( Node
          ( _newNamespace
          , newTagName
          , _newKey
          , newUnique
          , newProperties
          , newChildren ) as newNode ) ) -> (
      if oldUnique <> newUnique || oldTagName <> newTagName then
        let () = execUnmountHandlers oldCallbacks None oldNode in
        patchVNodesOnElemsReplaceNode oldCallbacks callbacks elem elems idx
          newNode
      else
        let child = nodeAt idx elems in
        match Webapi.Dom.Element.ofNode child with
        | None ->
            failwith "Expected element in patchVNodesOnElems_MutateNode"
        | Some childElement ->
            let childChildren = Webapi.Dom.Node.childNodes child in
            let () =
              if
                patchVNodesOnElemsProperties callbacks childElement
                  oldProperties newProperties
              then ()
              else
                let () =
                  Js.log
                    "VDom:  Failed swapping properties because the property \
                     list length changed, use `noProp` to swap properties \
                     instead, not by altering the list structure.  This is a \
                     massive inefficiency until this issue is resolved."
                in
                let () = execUnmountHandlers oldCallbacks None oldNode in
                patchVNodesOnElemsReplaceNode oldCallbacks callbacks elem elems
                  idx newNode
            in
            patchVNodesOnElems oldCallbacks callbacks child childChildren 0
              oldChildren newChildren )
  | _ ->
      failwith "Non-node passed to patchVNodesOnElemsMutateNode"

and patchVNodesOnElems (oldCallbacks : 'msg applicationCallbacks ref)
    (callbacks : 'msg applicationCallbacks ref) (elem : Dom.node)
    (elems : Dom.nodeList) (idx : int) (oldVNodes : 'msg t list)
    (newVNodes : 'msg t list) : unit =
  match[@ocaml.warning "-4"] (oldVNodes, newVNodes) with
  | ( Tagger (oldTagger, oldVdom) :: oldRest
    , Tagger (newTagger, newVdom) :: newRest ) ->
      let () =
        patchVNodesOnElems (oldTagger oldCallbacks) (newTagger callbacks) elem
          elems idx [oldVdom] [newVdom]
      in
      patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
        newRest
  | Tagger (oldTagger, oldVdom) :: oldRest, newNode :: newRest ->
      let oldChild = nodeAt idx elems in
      let newChild =
        patchVNodesOnElemsCreateElement oldCallbacks callbacks newNode
      in
      let attachedChild = Webapi.Dom.Node.insertBefore newChild oldChild elem in
      let _removedChild = Webapi.Dom.Node.removeChild oldChild elem in
      let () = execUnmountHandlers (oldTagger oldCallbacks) None oldVdom in
      let () =
        execMountHandlers callbacks
          (Webapi.Dom.Element.ofNode attachedChild)
          newNode
      in
      patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
        newRest
  | [], [] ->
      ()
  | [], newNode :: newRest ->
      let newChild =
        patchVNodesOnElemsCreateElement oldCallbacks callbacks newNode
      in
      let attachedChild = appendChild newChild elem in
      let () =
        execMountHandlers callbacks
          (Webapi.Dom.Element.ofNode attachedChild)
          newNode
      in
      patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) [] newRest
  | oldVnode :: oldRest, [] ->
      let child = nodeAt idx elems in
      let _removedChild = Webapi.Dom.Node.removeChild child elem in
      let () = execUnmountHandlers oldCallbacks None oldVnode in
      patchVNodesOnElems oldCallbacks callbacks elem elems idx oldRest []
  | CommentNode oldS :: oldRest, CommentNode newS :: newRest when oldS = newS ->
      patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
        newRest
  | Text oldText :: oldRest, Text newText :: newRest ->
      let () =
        if oldText = newText then ()
        else
          let child = nodeAt idx elems in
          Webapi.Dom.Node.setNodeValue child (Js.Null.return newText)
      in
      patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
        newRest
  | ( LazyGen (oldKey, _oldGen, oldCache) :: oldRest
    , LazyGen (newKey, newGen, newCache) :: newRest ) -> (
      if oldKey = newKey then
        let () = newCache := !oldCache in
        patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
          newRest
      else
        match (oldRest, newRest) with
        | ( LazyGen (olderKey, _olderGen, _olderCache) :: olderRest
          , LazyGen (newerKey, _newerGen, _newerCache) :: newerRest )
          when olderKey = newKey && oldKey = newerKey ->
            let firstChild = nodeAt idx elems in
            let secondChild = nodeAt (idx + 1) elems in
            let _removedChild = Webapi.Dom.Node.removeChild secondChild elem in
            let _attachedChild =
              Webapi.Dom.Node.insertBefore secondChild firstChild elem
            in
            patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 2)
              olderRest newerRest
        | LazyGen (olderKey, _olderGen, olderCache) :: olderRest, _
          when olderKey = newKey ->
            let oldChild = nodeAt idx elems in
            let _removedChild = Webapi.Dom.Node.removeChild oldChild elem in
            let oldVdom = !olderCache in
            let () = newCache := oldVdom in
            patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1)
              olderRest newRest
        | _, LazyGen (newerKey, _newerGen, _newerCache) :: _newerRest
          when newerKey = oldKey ->
            let oldChild = nodeAt idx elems in
            let newVdom = newGen () in
            let () = newCache := newVdom in
            let newChild =
              patchVNodesOnElemsCreateElement oldCallbacks callbacks newVdom
            in
            let attachedChild =
              Webapi.Dom.Node.insertBefore newChild oldChild elem
            in
            let () =
              execMountHandlers callbacks
                (Webapi.Dom.Element.ofNode attachedChild)
                newVdom
            in
            patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1)
              oldVNodes newRest
        | _ ->
            let oldVdom = !oldCache in
            let newVdom = newGen () in
            let () = newCache := newVdom in
            patchVNodesOnElems oldCallbacks callbacks elem elems idx
              (oldVdom :: oldRest) (newVdom :: newRest) )
  | ( ( Node
          ( oldNamespace
          , oldTagName
          , oldKey
          , _oldUnique
          , _oldProperties
          , _oldChildren ) as oldNode )
      :: oldRest
    , ( Node
          ( newNamespace
          , newTagName
          , newKey
          , _newUnique
          , _newProperties
          , _newChildren ) as newNode )
      :: newRest ) -> (
      if oldKey = newKey && oldKey <> "" then
        patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
          newRest
      else if oldKey = "" || newKey = "" then
        let () =
          patchVNodesOnElemsMutateNode oldCallbacks callbacks elem elems idx
            oldNode newNode
        in
        patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
          newRest
      else
        match (oldRest, newRest) with
        | ( ( Node
                ( olderNamespace
                , olderTagName
                , olderKey
                , _olderUnique
                , _olderProperties
                , _olderChildren ) as olderNode )
            :: olderRest
          , Node
              ( newerNamespace
              , newerTagName
              , newerKey
              , _newerUnique
              , _newerProperties
              , _newerChildren )
            :: newerRest )
          when olderNamespace = newNamespace
               && olderTagName = newTagName && olderKey = newKey
               && oldNamespace = newerNamespace
               && oldTagName = newerTagName && oldKey = newerKey ->
            let firstChild = nodeAt idx elems in
            let secondChild = nodeAt (idx + 1) elems in
            let _removedChild = Webapi.Dom.Node.removeChild secondChild elem in
            let () = execUnmountHandlers oldCallbacks None olderNode in
            let _attachedChild =
              Webapi.Dom.Node.insertBefore secondChild firstChild elem
            in
            patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 2)
              olderRest newerRest
        | ( Node
              ( olderNamespace
              , olderTagName
              , olderKey
              , _olderUnique
              , _olderProperties
              , _olderChildren )
            :: olderRest
          , _ )
          when olderNamespace = newNamespace
               && olderTagName = newTagName && olderKey = newKey ->
            let oldChild = nodeAt idx elems in
            let _removedChild = Webapi.Dom.Node.removeChild oldChild elem in
            let () = execUnmountHandlers oldCallbacks None oldNode in
            patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1)
              olderRest newRest
        | ( _
          , Node
              ( newerNamespace
              , newerTagName
              , newerKey
              , _newerUnique
              , _newerProperties
              , _newerChildren )
            :: _newerRest )
          when oldNamespace = newerNamespace
               && oldTagName = newerTagName && oldKey = newerKey ->
            let oldChild = nodeAt idx elems in
            let newChild =
              patchVNodesOnElemsCreateElement oldCallbacks callbacks newNode
            in
            let attachedChild =
              Webapi.Dom.Node.insertBefore newChild oldChild elem
            in
            let () =
              execMountHandlers callbacks
                (Webapi.Dom.Element.ofNode attachedChild)
                newNode
            in
            patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1)
              oldVNodes newRest
        | _ ->
            let () =
              patchVNodesOnElemsMutateNode oldCallbacks callbacks elem elems idx
                oldNode newNode
            in
            patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1)
              oldRest newRest )
  | oldVnode :: oldRest, newNode :: newRest ->
      let oldChild = nodeAt idx elems in
      let newChild =
        patchVNodesOnElemsCreateElement oldCallbacks callbacks newNode
      in
      let attachedChild = Webapi.Dom.Node.insertBefore newChild oldChild elem in
      let () =
        execMountHandlers callbacks
          (Webapi.Dom.Element.ofNode attachedChild)
          newNode
      in
      let _removedChild = Webapi.Dom.Node.removeChild oldChild elem in
      let () = execUnmountHandlers oldCallbacks None oldVnode in
      patchVNodesOnElems oldCallbacks callbacks elem elems (idx + 1) oldRest
        newRest

let patchVNodesIntoElement (callbacks : 'msg applicationCallbacks ref)
    (elem : Dom.node) (oldVNodes : 'msg t list) (newVNodes : 'msg t list) :
    'msg t list =
  let elems = Webapi.Dom.Node.childNodes elem in
  let () =
    patchVNodesOnElems callbacks callbacks elem elems 0 oldVNodes newVNodes
  in
  newVNodes

let patchVNodeIntoElement (callbacks : 'msg applicationCallbacks ref)
    (elem : Dom.node) (oldVNode : 'msg t) (newVNode : 'msg t) : 'msg t list =
  patchVNodesIntoElement callbacks elem [oldVNode] [newVNode]

let wrapCallbacksOn : type a b. (a -> b) -> a systemMessage -> b systemMessage =
 fun func x ->
  match x with
  | Render ->
      Render
  | AddRenderMsg msg ->
      AddRenderMsg (func msg)
  | RemoveRenderMsg msg ->
      RemoveRenderMsg (func msg)

let wrapCallbacks :
    type a b.
    (a -> b) -> b applicationCallbacks ref -> a applicationCallbacks ref =
 fun func callbacks ->
  Obj.magic ref
    { enqueue=
        (fun (msg : a) ->
          let new_msg = func msg in
          !callbacks.enqueue new_msg )
    ; on=
        (fun smsg ->
          let new_smsg = wrapCallbacksOn func smsg in
          !callbacks.on new_smsg ) }

let (map : ('a -> 'b) -> 'a t -> 'b t) =
  ( fun func vdom ->
      let tagger = wrapCallbacks func in
      Tagger (Obj.magic tagger, Obj.magic vdom)
    : ('a -> 'b) -> 'a t -> 'b t )
