(** Application with the time travel debugger
    @canonical Tea.Debug
*)

(**/**)

type nonrec 'msg debugMsg =
  | ClientMsg of 'msg
  | TogglePaused
  | SelectHistoryItem of int
  | ToggleDetails

val clientMsg : 'msg -> 'msg debugMsg

type nonrec state = Running | Paused of int

type nonrec 'model debugModel =
  {history: (string * 'model) list; state: state; showDetails: bool}

val debug :
     ('msg -> string)
  -> ('model -> 'msg -> 'model * 'msg Tea_cmd.t)
  -> ('model -> 'msg Vdom.t)
  -> ('model -> 'msg Tea_sub.t)
  -> ('model -> 'msg Tea_cmd.t)
  -> ('model * 'msg Tea_cmd.t -> 'model debugModel * 'msg debugMsg Tea_cmd.t)
     * (   'model debugModel
        -> 'msg debugMsg
        -> 'model debugModel * 'msg debugMsg Tea_cmd.t )
     * ('model debugModel -> 'msg debugMsg Vdom.t)
     * ('model debugModel -> 'msg debugMsg Tea_sub.t)
     * ('model debugModel -> 'msg debugMsg Tea_cmd.t)

val debugProgram :
     ('msg -> string)
  -> ('flags, 'model, 'msg) Tea_app.program
  -> ('flags, 'model debugModel, 'msg debugMsg) Tea_app.program

val debugNavigationProgram :
     ('msg -> string)
  -> ('flags, 'model, 'msg) Tea_navigation.navigationProgram
  -> ('flags, 'model debugModel, 'msg debugMsg) Tea_navigation.navigationProgram

(**/**)

val beginnerProgram :
     ('model, 'msg) Tea_app.beginnerProgram
  -> ('msg -> string)
  -> Dom.node option
  -> unit
  -> 'msg debugMsg Tea_app.programInterface

val standardProgram :
     ('flags, 'model, 'msg) Tea_app.standardProgram
  -> ('msg -> string)
  -> Dom.node option
  -> 'flags
  -> 'msg debugMsg Tea_app.programInterface

val program :
     ('flags, 'model, 'msg) Tea_app.program
  -> ('msg -> string)
  -> Dom.node option
  -> 'flags
  -> 'msg debugMsg Tea_app.programInterface

val navigationProgram :
     (Tea_navigation.Location.t -> 'msg)
  -> ('flags, 'model, 'msg) Tea_navigation.navigationProgram
  -> ('msg -> string)
  -> Dom.node option
  -> 'flags
  -> 'msg debugMsg Tea_app.programInterface
