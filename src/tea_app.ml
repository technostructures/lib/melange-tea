type nonrec ('flags, 'model, 'msg) program =
  { init: 'flags -> 'model * 'msg Tea_cmd.t
  ; update: 'model -> 'msg -> 'model * 'msg Tea_cmd.t
  ; view: 'model -> 'msg Vdom.t
  ; subscriptions: 'model -> 'msg Tea_sub.t
  ; shutdown: 'model -> 'msg Tea_cmd.t }

type nonrec ('flags, 'model, 'msg) standardProgram =
  { init: 'flags -> 'model * 'msg Tea_cmd.t
  ; update: 'model -> 'msg -> 'model * 'msg Tea_cmd.t
  ; view: 'model -> 'msg Vdom.t
  ; subscriptions: 'model -> 'msg Tea_sub.t }

type nonrec ('model, 'msg) beginnerProgram =
  {model: 'model; update: 'model -> 'msg -> 'model; view: 'model -> 'msg Vdom.t}

type nonrec ('model, 'msg) pumpInterface =
  { startup: unit -> unit
  ; renderString: 'model -> string
  ; handleMsg: 'model -> 'msg -> 'model
  ; shutdown: 'msg Tea_cmd.t -> unit }

type nonrec 'msg programInterface =
  < pushMsg: 'msg -> unit
  ; shutdown: unit -> unit
  ; getHtmlString: unit -> string >
  Js.t

external makeProgramInterface :
     pushMsg:('msg -> unit)
  -> shutdown:(unit -> unit)
  -> getHtmlString:(unit -> string)
  -> 'msg programInterface = ""
[@@mel.obj]

let programStateWrapper initModel pump shutdown =
  let open Vdom in
  let model = ref initModel in
  let callbacks =
    ref
      {enqueue= (fun _msg -> Js.log "INVALID enqueue CALL!"); on= (fun _ -> ())}
  in
  let pumperInterfaceC () = pump callbacks in
  let pumperInterface = pumperInterfaceC () in
  let (pending : 'msg list option ref) = (ref None : 'msg list option ref) in
  let rec handler msg =
    match !pending with
    | None -> (
        let () = pending := Some [] in
        let newModel = pumperInterface.handleMsg !model msg in
        let () = model := newModel in
        match !pending with
        | None ->
            failwith
              "INVALID message queue state, should never be None during \
               message processing!"
        | Some [] ->
            pending := None
        | Some msgs ->
            let () = pending := None in
            List.iter handler (List.rev msgs) )
    | Some msgs ->
        pending := Some (msg :: msgs)
  in
  let renderEvents = ref [] in
  let (finalizedCBs : 'msg Vdom.applicationCallbacks) =
    ( { enqueue= (fun msg -> handler msg)
      ; on=
          (fun x ->
            match x with
            | Render ->
                List.iter handler !renderEvents
            | AddRenderMsg msg ->
                renderEvents := List.append !renderEvents [msg]
            | RemoveRenderMsg msg ->
                renderEvents := List.filter (fun mg -> msg != mg) !renderEvents
            ) }
      : 'msg Vdom.applicationCallbacks )
  in
  let () = callbacks := finalizedCBs in
  let piRequestShutdown () =
    let () =
      callbacks :=
        { enqueue= (fun _msg -> Js.log "INVALID message enqueued when shut down")
        ; on= (fun _ -> ()) }
    in
    let cmd = shutdown !model in
    let () = pumperInterface.shutdown cmd in
    ()
  in
  let renderString () =
    let rendered = pumperInterface.renderString !model in
    rendered
  in
  let () = pumperInterface.startup () in
  makeProgramInterface ~pushMsg:handler ~shutdown:piRequestShutdown
    ~getHtmlString:renderString

let programLoop update view subscriptions initModel initCmd x =
  match x with
  | None ->
      fun callbacks ->
        let oldSub = ref Tea_sub.none in
        let handleSubscriptionChange model =
          let newSub = subscriptions model in
          oldSub := Tea_sub.run callbacks callbacks !oldSub newSub
        in
        { startup=
            (fun () ->
              let () = Tea_cmd.run callbacks initCmd in
              let () = handleSubscriptionChange initModel in
              () )
        ; renderString=
            (fun model ->
              let vdom = view model in
              let rendered = Vdom.renderToHtmlString vdom in
              rendered )
        ; handleMsg=
            (fun model msg ->
              let newModel, cmd = update model msg in
              let () = Tea_cmd.run callbacks cmd in
              let () = handleSubscriptionChange newModel in
              newModel )
        ; shutdown=
            (fun cmd ->
              let () = Tea_cmd.run callbacks cmd in
              let () =
                oldSub := Tea_sub.run callbacks callbacks !oldSub Tea_sub.none
              in
              () ) }
  | Some parentNode ->
      fun callbacks ->
        let priorRenderedVdom = ref [] in
        let latestModel = ref initModel in
        let noFrameID = Some (Obj.magic (-1)) in
        let (nextFrameID : Webapi.rafId option ref) = ref None in
        let doRender _delta =
          match !nextFrameID with
          | None ->
              ()
          | Some _id ->
              let newVdom = [view !latestModel] in
              let justRenderedVdom =
                Vdom.patchVNodesIntoElement callbacks parentNode
                  !priorRenderedVdom newVdom
              in
              let () = priorRenderedVdom := justRenderedVdom in
              let () = !callbacks.on Render in
              nextFrameID := None
        in
        let scheduleRender () =
          match !nextFrameID with
          | Some _ ->
              ()
          | None ->
              (* @TODO: add an optional parameters in program functions for realtime rendering *)
              let realtimeRendering = false in
              if realtimeRendering then
                let () = nextFrameID := noFrameID in
                doRender 16
              else
                let id = Webapi.requestCancellableAnimationFrame doRender in
                let () = nextFrameID := Some id in
                ()
        in
        let clearPnode () =
          while
            Webapi.Dom.NodeList.length (Webapi.Dom.Node.childNodes parentNode)
            > 0
          do
            match Webapi.Dom.Node.firstChild parentNode with
            | None ->
                ()
            | Some firstChild ->
                let _removedChild =
                  Webapi.Dom.Node.removeChild firstChild parentNode
                in
                ()
          done
        in
        let oldSub = ref Tea_sub.none in
        let handleSubscriptionChange model =
          let newSub = subscriptions model in
          oldSub := Tea_sub.run callbacks callbacks !oldSub newSub
        in
        let handlerStartup () =
          let () = clearPnode () in
          let () = Tea_cmd.run callbacks initCmd in
          let () = handleSubscriptionChange !latestModel in
          let () = nextFrameID := noFrameID in
          let () = doRender 16 in
          ()
        in
        let renderString model =
          let vdom = view model in
          let rendered = Vdom.renderToHtmlString vdom in
          rendered
        in
        let handler model msg =
          let newModel, cmd = update model msg in
          let () = latestModel := newModel in
          let () = Tea_cmd.run callbacks cmd in
          let () = scheduleRender () in
          let () = handleSubscriptionChange newModel in
          newModel
        in
        let handlerShutdown cmd =
          let () = nextFrameID := None in
          let () = Tea_cmd.run callbacks cmd in
          let () =
            oldSub := Tea_sub.run callbacks callbacks !oldSub Tea_sub.none
          in
          let () = priorRenderedVdom := [] in
          let () = clearPnode () in
          ()
        in
        { startup= handlerStartup
        ; renderString
        ; handleMsg= handler
        ; shutdown= handlerShutdown }

let (program :
         ('flags, 'model, 'msg) program
      -> Dom.node option
      -> 'flags
      -> 'msg programInterface ) =
  ( fun {init; update; view; subscriptions; shutdown} opnode flags ->
      let initModel, initCmd = init flags in
      let pumpInterface =
        programLoop update view subscriptions initModel initCmd opnode
      in
      programStateWrapper initModel pumpInterface shutdown
    :    ('flags, 'model, 'msg) program
      -> Dom.node option
      -> 'flags
      -> 'msg programInterface )

let (standardProgram :
         ('flags, 'model, 'msg) standardProgram
      -> Dom.node option
      -> 'flags
      -> 'msg programInterface ) =
  ( fun {init; update; view; subscriptions} pnode args ->
      program
        { init
        ; update
        ; view
        ; subscriptions
        ; shutdown= (fun _model -> Tea_cmd.none) }
        pnode args
    :    ('flags, 'model, 'msg) standardProgram
      -> Dom.node option
      -> 'flags
      -> 'msg programInterface )

let (beginnerProgram :
         ('model, 'msg) beginnerProgram
      -> Dom.node option
      -> unit
      -> 'msg programInterface ) =
  ( fun {model; update; view} pnode () ->
      standardProgram
        { init= (fun () -> (model, Tea_cmd.none))
        ; update= (fun model msg -> (update model msg, Tea_cmd.none))
        ; view
        ; subscriptions= (fun _model -> Tea_sub.none) }
        pnode ()
    :    ('model, 'msg) beginnerProgram
      -> Dom.node option
      -> unit
      -> 'msg programInterface )

let map func vnode = Vdom.map func vnode
