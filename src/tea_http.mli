(** Functions for HTTP-related commands
    @canonical Tea.Http
*)

type nonrec responseStatus = {code: int; message: string}

type nonrec requestBody = Web.XMLHttpRequest.body

type nonrec bodyType = Web.XMLHttpRequest.responseType

type nonrec responseBody = Web.XMLHttpRequest.responseBody

type nonrec response =
  { url: string
  ; status: responseStatus
  ; headers: string Belt.Map.String.t
  ; body: responseBody }

type nonrec 'parsedata error =
  | BadUrl of string
  | Timeout
  | NetworkError
  | Aborted
  | BadStatus of response
  | BadPayload of 'parsedata * response

val stringOfError : 'msg error -> string

type nonrec header = Header of string * string

type nonrec 'res expect =
  | Expect of bodyType * (response -> ('res, string) result)

type nonrec 'msg requestEvents =
  { onreadystatechange:
      (   'msg Vdom.applicationCallbacks ref
       -> Web.XMLHttpRequest.eventReadystatechange
       -> unit )
      option
  ; onprogress:
      (   'msg Vdom.applicationCallbacks ref
       -> Web.XMLHttpRequest.eventProgress
       -> unit )
      option }

val emptyRequestEvents : 'a requestEvents

type nonrec 'res rawRequest =
  { method': string
  ; headers: header list
  ; url: string
  ; body: requestBody
  ; expect: 'res expect
  ; timeout: Tea_time.t option
  ; withCredentials: bool }

type nonrec ('msg, 'res) request =
  | Request of 'res rawRequest * 'msg requestEvents option

val expectStringResponse : (string -> ('a, string) result) -> 'a expect

val expectString : string expect

val request : 'a rawRequest -> ('b, 'a) request

val getString : string -> ('a, string) request

val toTask : ('a, 'b) request -> ('b, string error) Tea_task.t

val send : (('a, string error) result -> 'b) -> ('b, 'a) request -> 'b Tea_cmd.t

external encodeURIComponent : string -> string = "encodeURIComponent"

val encodeUri : string -> string

external decodeURIComponent : string -> string = "decodeURIComponent"

val decodeUri : string -> string option

module Progress : sig
  type nonrec t = {bytes: int; bytesExpected: int}

  val emptyProgress : t

  val track : (t -> 'a) -> ('a, 'b) request -> ('a, 'b) request
end
