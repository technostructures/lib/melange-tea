let renderEvent ?(key = "") msg =
  let open Vdom in
  let enableCall callbacks =
    let () = callbacks.on (AddRenderMsg msg) in
    fun () -> callbacks.on (RemoveRenderMsg msg)
  in
  Tea_sub.registration key enableCall

module LocalStorage = struct
  let nativeBinding = Tea_task.nativeBinding

  let (length : (int, string) Tea_task.t) =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage.length Dom.Storage.localStorage)) )

  let (clear : (unit, string) Tea_task.t) =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage.clear Dom.Storage.localStorage)) )

  let clearCmd () = Tea_task.attemptOpt (fun _ -> None) clear

  let key idx : (string option, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage.key idx Dom.Storage.localStorage)) )

  let getItem key : (string option, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage.getItem key Dom.Storage.localStorage)) )

  let removeItem key : (unit, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage.removeItem key Dom.Storage.localStorage)) )

  let removeItemCmd key = Tea_task.attemptOpt (fun _ -> None) (removeItem key)

  let setItem key value : (unit, string) Tea_task.t =
    nativeBinding (fun cb ->
        cb (Ok (Dom.Storage.setItem key value Dom.Storage.localStorage)) )

  let setItemCmd key value =
    Tea_task.attemptOpt (fun _ -> None) (setItem key value)
end
