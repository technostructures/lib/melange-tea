(** Helper functions for SVG
    @canonical Tea.Svg
*)

val svgNamespace : string

val noNode : 'msg Vdom.t

val text : string -> 'msg Vdom.t

val lazy1 : string -> (unit -> 'msg Vdom.t) -> 'msg Vdom.t

val node :
     string
  -> ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val svg :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val foreignObject :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animate :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animateColor :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animateMotion :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val animateTransform :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val mpath :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val set :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val a :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val defs :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val g :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val marker :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val mask :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val missingGlyph :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val pattern :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val switch :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val symbol :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val desc :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val metadata :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val title :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feBlend :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feColorMatrix :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feComponentTransfer :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feComposite :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feConvolveMatrix :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feDiffuseLighting :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feDisplacementMap :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFlood :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncA :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncB :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncG :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feFuncR :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feGaussianBlur :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feImage :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feMerge :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feMergeNode :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feMorphology :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feOffset :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feSpecularLighting :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feTile :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feTurbulence :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val font :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFace :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceFormat :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceName :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceSrc :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fontFaceUri :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val hkern :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val vkern :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val linearGradient :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val radialGradient :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val stop :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val circle :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val ellipse :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val svgimage :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val line :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val path :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val polygon :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val polyline :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val rect :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val use :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feDistantLight :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val fePointLight :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val feSpotLight :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val altGlyph :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val altGlyphDef :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val altGlyphItem :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val glyph :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val glyphRef :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val textPath :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val text' :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val tref :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val tspan :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val clipPath :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val svgcolorProfile :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val cursor :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val filter :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val script :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val style :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t

val view :
     ?key:string
  -> ?unique:string
  -> 'msg Vdom.properties
  -> 'msg Vdom.t list
  -> 'msg Vdom.t
