(** Tasks
    @canonical Tea.Task
*)

type nonrec never

type ('succeed, 'fail) t =
  | Task : ((('succeed, 'fail) result -> unit) -> unit) -> ('succeed, 'fail) t

val nothing : unit -> unit

val performOpt : ('value -> 'msg option) -> ('value, never) t -> 'msg Tea_cmd.t

val perform : ('value -> 'msg) -> ('value, never) t -> 'msg Tea_cmd.t

val attemptOpt :
     (('succeed, 'fail) Belt.Result.t -> 'msg option)
  -> ('succeed, 'fail) t
  -> 'msg Tea_cmd.t

val attempt :
  (('succeed, 'fail) result -> 'msg) -> ('succeed, 'fail) t -> 'msg Tea_cmd.t

val ignore : ('msg, 'b) t -> 'c Tea_cmd.t

val succeed : 'v -> ('v, 'e) t

val fail : 'v -> ('e, 'v) t

val nativeBinding :
  ((('succeed, 'fail) Belt.Result.t -> unit) -> unit) -> ('succeed, 'fail) t

val andThen : ('msg -> ('b, 'c) t) -> ('msg, 'c) t -> ('b, 'c) t

val onError : ('msg -> ('b, 'c) t) -> ('b, 'msg) t -> ('b, 'c) t

val fromResult : ('success, 'failure) result -> ('success, 'failure) t

val mapError : ('msg -> 'b) -> ('c, 'msg) t -> ('c, 'b) t

val toOption : ('msg, 'b) t -> ('msg option, 'c) t

val map : ('msg -> 'b) -> ('msg, 'c) t -> ('b, 'c) t

val map2 : ('msg -> 'b -> 'c) -> ('msg, 'd) t -> ('b, 'd) t -> ('c, 'd) t

val map3 :
     ('msg -> 'b -> 'c -> 'd)
  -> ('msg, 'e) t
  -> ('b, 'e) t
  -> ('c, 'e) t
  -> ('d, 'e) t

val map4 :
     ('msg -> 'b -> 'c -> 'd -> 'e)
  -> ('msg, 'f) t
  -> ('b, 'f) t
  -> ('c, 'f) t
  -> ('d, 'f) t
  -> ('e, 'f) t

val map5 :
     ('msg -> 'b -> 'c -> 'd -> 'e -> 'f)
  -> ('msg, 'g) t
  -> ('b, 'g) t
  -> ('c, 'g) t
  -> ('d, 'g) t
  -> ('e, 'g) t
  -> ('f, 'g) t

val map6 :
     ('msg -> 'b -> 'c -> 'd -> 'e -> 'f -> 'g)
  -> ('msg, 'h) t
  -> ('b, 'h) t
  -> ('c, 'h) t
  -> ('d, 'h) t
  -> ('e, 'h) t
  -> ('f, 'h) t
  -> ('g, 'h) t

val sequence : ('msg, 'b) t list -> ('msg list, 'b) t

val testing_deop : bool ref

val testing : unit -> unit
