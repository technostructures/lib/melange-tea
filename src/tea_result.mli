(** Helper functions for working with results
    @canonical Tea.Result
*)

val first : ('msg, 'b) result -> ('c, 'b) result -> ('msg, 'b) result

val resultToOption : ('msg, 'b) result -> 'msg option

val errorOfFirst : ('a, 'b) result -> ('c, 'b) result -> 'b option
