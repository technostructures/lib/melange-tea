type nonrec unresolved

type nonrec xmlHttpRequestUpload

type nonrec eventReadystatechange = Js.Json.t

type nonrec eventAbort = Js.Json.t

type nonrec eventError = Js.Json.t

type nonrec eventLoad = Js.Json.t

type nonrec eventLoadstart = Js.Json.t

type nonrec eventProgress = Js.Json.t

type nonrec eventTimeout = Js.Json.t

type nonrec eventLoadend = Js.Json.t

type nonrec _xmlhttprequest =
  < onreadystatechange: eventReadystatechange -> unit [@mel.get] [@mel.set]
  ; readyState: int [@mel.get]
  ; responseType: string [@mel.get] [@mel.set]
  ; response: unresolved Js.null
  ; responseText: string [@mel.get]
  ; responseURL: string [@mel.get]
  ; responseXML: Dom.document Js.null [@mel.get]
  ; status: int [@mel.get]
  ; statusText: string [@mel.get]
  ; timeout: float [@mel.get] [@mel.set]
  ; upload: xmlHttpRequestUpload [@mel.get]
  ; withCredentials: bool [@mel.get] [@mel.set]
  ; onabort: eventAbort -> unit [@mel.get] [@mel.set]
  ; onerror: eventError -> unit [@mel.get] [@mel.set]
  ; onload: eventLoad -> unit [@mel.get] [@mel.set]
  ; onloadstart: eventLoadstart -> unit [@mel.get] [@mel.set]
  ; onprogress: eventProgress -> unit [@mel.get] [@mel.set]
  ; ontimeout: eventTimeout -> unit [@mel.get] [@mel.set]
  ; onloadend: eventLoadend -> unit [@mel.get] [@mel.set] >
  Js.t

type nonrec t = _xmlhttprequest

external getResponseHeader : t -> string Js.null = "getResponseHeader"
[@@mel.send]

external sendString : t -> string Js.null -> unit = "send" [@@mel.send]

external sendFormData : t -> Webapi.FormData.t -> unit = "send" [@@mel.send]

external sendDocument : t -> Dom.document -> unit = "send" [@@mel.send]

external create : unit -> t = "XMLHttpRequest" [@@mel.new]

type nonrec errors = IncompleteResponse | NetworkError

type nonrec body =
  | EmptyBody
  | EmptyStringBody
  | StringBody of string
  | FormDataBody of Webapi.FormData.t
  | FormListBody of (string * string) list
  | DocumentBody of Dom.document

val abort : t -> unit

val getAllResponseHeaders : t -> (string, errors) result

val getAllResponseHeadersAsList : t -> ((string * string) list, errors) result

val getAllResponseHeadersAsDict : t -> (string Belt.Map.String.t, errors) result

val open' :
     string
  -> string
  -> ?async:bool
  -> ?user:string
  -> ?password:string
  -> t
  -> unit

val overrideMimeType : string -> t -> unit

val send : body -> t -> unit

val setRequestHeader : string -> string -> t -> unit

type nonrec state = Unsent | Opened | HeadersReceived | Loading | Done

type nonrec responseType =
  | StringResponseType
  | ArrayBufferResponseType
  | BlobResponseType
  | DocumentResponseType
  | JsonResponseType
  | TextResponseType
  | RawResponseType of string

type nonrec responseBody =
  | NoResponse
  | StringResponse of string
  | ArrayBufferResponse of unit
  | BlobResponse of unit
  | DocumentResponse of Dom.document
  | JsonResponse of Js.Json.t
  | TextResponse of string
  | RawResponse of string * unit

val set_onreadystatechange : (eventReadystatechange -> unit) -> t -> unit

val get_onreadystatechange : t -> eventReadystatechange -> unit

val readyState : t -> state

val set_responseType : responseType -> t -> unit

val get_responseType : t -> responseType

val get_response : t -> responseBody

val get_responseText : t -> string

val get_responseURL : t -> string

val get_responseXML : t -> Dom.document option

val get_status : t -> int

val get_statusText : t -> string

val set_timeout : float -> t -> unit

val get_timeout : t -> float

val set_withCredentials : bool -> t -> unit

val get_withCredentials : t -> bool

val set_onabort : (eventAbort -> unit) -> t -> unit

val get_onabort : t -> eventAbort -> unit

val set_onerror : (eventError -> unit) -> t -> unit

val get_onerror : t -> eventError -> unit

val set_onload : (eventLoad -> unit) -> t -> unit

val get_onload : t -> eventLoad -> unit

val set_onloadstart : (eventLoadstart -> unit) -> t -> unit

val get_onloadstart : t -> eventLoadstart -> unit

val set_onprogress : (eventLoadstart -> unit) -> t -> unit

val get_onprogress : t -> eventLoadstart -> unit

val set_ontimeout : (eventTimeout -> unit) -> t -> unit

val get_ontimeout : t -> eventTimeout -> unit

val set_onloadend : (eventLoadend -> unit) -> t -> unit

val get_onloadend : t -> eventLoadend -> unit
