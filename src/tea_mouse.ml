type nonrec position = {x: int; y: int}

let position =
  let open Tea_json.Decoder in
  map2 (fun x y -> {x; y}) (field "pageX" int) (field "pageY" int)

let registerGlobal name key tagger =
  let open Vdom in
  let enableCall callbacks_base =
    let callbacks = ref callbacks_base in
    let fn ev =
      let open Tea_json.Decoder in
      match decodeEvent position ev with
      | Error _ ->
          None
      | Ok pos ->
          Some (tagger pos)
    in
    let handler = EventHandlerCallback (key, fn) in
    let eventTarget =
      Webapi.Dom.document |> Webapi.Dom.Document.asEventTarget
    in
    let cache = eventHandlerRegister callbacks eventTarget name handler in
    fun () ->
      let _ = eventHandlerUnregister eventTarget name cache in
      ()
  in
  Tea_sub.registration key enableCall

let clicks ?(key = "") tagger = registerGlobal "click" key tagger

let moves ?(key = "") tagger = registerGlobal "mousemove" key tagger

let downs ?(key = "") tagger = registerGlobal "mousedown" key tagger

let ups ?(key = "") tagger = registerGlobal "mouseup" key tagger
