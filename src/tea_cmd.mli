(** Helper functions for commands
    @canonical Tea.Cmd
*)

type nonrec 'msg applicationCallbacks = 'msg Vdom.applicationCallbacks

type 'msg t =
  | NoCmd : 'msg t
  | Mapper :
      ('msg Vdom.applicationCallbacks ref -> 'msgB Vdom.applicationCallbacks ref)
      * 'msgB t
      -> 'msg t
  | Batch : 'msg t list -> 'msg t
  | EnqueueCall : ('msg applicationCallbacks ref -> unit) -> 'msg t

val none : 'msg t

val batch : 'msg t list -> 'msg t

val call : ('msg applicationCallbacks ref -> unit) -> 'msg t

val fnMsg : (unit -> 'msg) -> 'msg t

val msg : 'msg -> 'msg t

val run : 'msg applicationCallbacks ref -> 'msg t -> unit

val map : ('msg -> 'b) -> 'msg t -> 'b t
