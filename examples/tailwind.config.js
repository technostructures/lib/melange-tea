export default {
    darkMode: 'class',
    content: [
        "tailwind-counter/index.html",
        "tailwind-counter/**/*.{js,ts,jsx,tsx,ml,re}",
    ],
    theme: {
      extend: {},
    },
    plugins: [],
}
