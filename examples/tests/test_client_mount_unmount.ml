open Tea.App

type sub_key = Foo | Bar

type sub_msg = Mounted | Unmounted | MountedCB | UnmountedCB
[@@deriving accessors]

type ignored_msg = Ignore

type msg =
  | Click
  | SetFoo
  | SetBar
  | SubMsg of sub_key * sub_msg
  | IgnoredMsg of ignored_msg
[@@deriving accessors]

type sub_model = {key: string}

type model = {foo: sub_model; bar: sub_model; current: sub_key}

let init () = {foo= {key= "foo"}; bar= {key= "bar"}; current= Foo}

let sub_update sub_model = function
  | Mounted ->
      let () = Js.log "Mounted" in
      let () = Js.log sub_model.key in
      sub_model
  | Unmounted ->
      let () = Js.log "Unmounted" in
      let () = Js.log sub_model.key in
      sub_model
  | MountedCB ->
      let () = Js.log "MountedCB" in
      let () = Js.log sub_model.key in
      sub_model
  | UnmountedCB ->
      let () = Js.log "UnmountedCB" in
      let () = Js.log sub_model.key in
      sub_model

let update model = function
  | Click ->
      model
  | SetFoo ->
      {model with current= Foo}
  | SetBar ->
      {model with current= Bar}
  | SubMsg (Foo, sub_msg) ->
      let foo = sub_update model.foo sub_msg in
      {model with foo}
  | SubMsg (Bar, sub_msg) ->
      let bar = sub_update model.bar sub_msg in
      {model with bar}
  | IgnoredMsg _ ->
      model

external is_connected : Dom.element -> bool = "isConnected" [@@mel.get]

let sub_view sub_model =
  let open! Tea.Html in
  div []
    [ div ~unique:sub_model.key
        [ Vdom.onMountMsg Mounted
        ; Vdom.onUnmountMsg Unmounted
        ; Vdom.onMountCB (function
            | Some elem ->
                (* let () = Js.log elem in *)
                let () = Js.log "inside mount CB" in
                let () = Js.log elem in
                let () = Js.log (is_connected elem) in
                Some MountedCB
            | None ->
                let () = Js.log "should not happen" in
                Some MountedCB )
        ; Vdom.onUnmountCB (function
            | Some _elem ->
                let () = Js.log "should not happen" in
                Some UnmountedCB
            | None ->
                let () = Js.log "inside unmount CB" in
                Some UnmountedCB ) ]
        [text sub_model.key] ]

let view model =
  let open! Tea.Html in
  let open Tea.Html.Events in
  let ignored_sub () = Vdom.map ignoredMsg (div [] []) in
  let sub () =
    match model.current with
    | Foo ->
        Vdom.map (subMsg Foo) (sub_view model.foo)
    | Bar ->
        Vdom.map (subMsg Bar) (sub_view model.bar)
  in
  div []
    [ button [onClick Click] [text "onClick"]
    ; button [onClick SetFoo] [text "set foo"]
    ; button [onClick SetBar] [text "set bar"]
    ; ignored_sub ()
    ; sub () ]

let main = beginnerProgram {model= init (); update; view}
