open Tea.App

type nonrec msg = Click | Set_value of int [@@deriving accessors]

let update model x = match x with Click -> model + 1 | Set_value n -> n

let view model =
  ((let open! Tea.Html in
    let open Tea.Html.Attributes in
    let open Tea.Html.Events in
    let open Tea.Json in
    let clientX = Decoder.field "clientX" Decoder.int in
    div []
      (List.map
         (fun e -> div [] [e])
         [ model |. Belt.Int.toString |. text
         ; button [onClick Click] [text "onClick"]
         ; button
             [on ~key:("" [@ns.namedArgLoc]) "click" (Decoder.succeed Click)]
             [text "on \"click\""]
         ; a [href "https://www.google.com"] [text "a normal link"]
         ; a
             [ href "https://www.google.com"
             ; onWithOptions ~key:("" [@ns.namedArgLoc]) "click"
                 {defaultOptions with preventDefault= true}
                 (Tea.Json.Decoder.succeed Click) ]
             [text "a link with prevent default"]
         ; button
             [ on ~key:("" [@ns.namedArgLoc]) "click"
                 (Decoder.map set_value clientX) ]
             [text "on \"click\", use clientX value"]
         ; input'
             [ type' "text"
             ; on ~key:("" [@ns.namedArgLoc]) "input"
                 (Decoder.map
                    (fun v -> v |> int_of_string |> set_value)
                    targetValue ) ]
             [] ] ) ) )

let main = beginnerProgram {model= 0; update; view}
