open Tea.App
open Tea.Html
open Tea.Html.Events

type nonrec msg = Increment | Decrement | Reset | Set of int

let string_of_msg x =
  match x with
  | Increment -> "Increment"
  | Decrement -> "Decrement"
  | Reset -> "Reset"
  | Set _ -> "Set"

let init () = (4, Tea.Cmd.none)

let subscriptions _ = Tea.Sub.none

let update model x =
  match x with
  | Increment -> (model + 1, Tea.Cmd.none)
  | Decrement -> (model - 1, Tea.Cmd.none)
  | Reset -> (0, Tea.Cmd.none)
  | Set v -> (v, Tea.Cmd.none)

let view_button title msg =
  button [Attributes.class' "pure-button"; onClick msg] [text title]

let view model =
  section
    [Attributes.class' "is-center"]
    [ h2 [] [text (Belt.Int.toString model)]
    ; div
        [Attributes.class' "pure-g"]
        [ div
            [Attributes.class' "pure-u p-4"]
            [ view_button "Increment"
                (if model >= 3 then Decrement else Increment) ]
        ; div
            [Attributes.class' "pure-u p-4"]
            [view_button "Decrement" Decrement]
        ; div
            [Attributes.class' "pure-u p-4"]
            [view_button "Set to 42" (Set 42)]
        ; div
            [Attributes.class' "pure-u p-4"]
            [(if model <> 0 then view_button "Reset" Reset else noNode)] ] ]

let main =
  Tea.Debug.program
    { init
    ; update
    ; view
    ; subscriptions
    ; shutdown= (fun _model -> Tea.Cmd.none) }
    string_of_msg
