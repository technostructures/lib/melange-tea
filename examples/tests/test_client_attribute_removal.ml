open Tea.App
open Tea.Html
open Tea.Html.Events
open Tea.Html.Attributes

type nonrec model = {selected: string option; languages: string list}

type nonrec message = Select of string | Delete [@@deriving accessors]

let render_selected x =
  match x with
  | Some selected ->
      p []
        [ span [class' "p-4"] [text ("You selected : " ^ selected)]
        ; button
            [onClick Delete; class' "pure-button"]
            [text ("Delete " ^ selected)] ]
  | None -> div [] [text "Nothing selected"]

let lang l is_selected =
  li
    [ onClick (Select l)
    ; ( if is_selected then class' "pure-button pure-button-primary"
        else class' "pure-button" ) ]
    [text l]

let render_languages selected languages =
  let is_selected selected language =
    match selected with Some l -> language == l | None -> false
  in
  let rendered =
    List.map (fun l -> lang l (is_selected selected l)) languages
  in
  ul [] rendered

let update state x =
  match x with
  | Select lang -> {state with selected= Some lang}
  | Delete -> {state with selected= None}

let view state =
  section []
    [ render_selected state.selected
    ; render_languages state.selected state.languages ]

let main =
  (let initialState =
     {selected= Some "Erlang"; languages= ["Erlang"; "Ocaml"; "Clojure"]}
   in
   beginnerProgram {model= initialState; update; view} )
