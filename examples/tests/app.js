import "./app.css";
import * as tests from "./test_client.ml";

var elem = document.getElementById("app");
var app = undefined;
function loadApp(name) {
  if (app) {
    console.log("Shutting down app");
    app.shutdown();
    elem.className = "app";
    app = undefined;
  }
  if (name) {
    console.log("Loading app:", name);
    elem.className = "app-" + name;
    app = tests[name](elem, 0);
  }
}
window.loadApp = loadApp;
