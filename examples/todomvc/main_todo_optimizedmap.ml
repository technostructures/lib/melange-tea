open Tea
open App
open Html
open Html.Events
module Cmds = Tea_html_cmds

module IntMap = Map.Make (struct
  type nonrec t = int

  let (compare : int -> int -> int) = compare
end)

type nonrec entry =
  {description: string; completed: bool; editing: bool; id: int}

type nonrec model =
  {entries: entry IntMap.t; field: string; uid: int; visibility: string}

let emptyModel = {entries= IntMap.empty; visibility= "All"; field= ""; uid= 0}

let newEntry desc id = {description= desc; completed= false; editing= false; id}

let init () = (emptyModel, Cmd.none)

type nonrec msg =
  | UpdateField of string
  | EditingEntry of int * bool
  | UpdateEntry of int * string
  | Add
  | Delete of int
  | DeleteComplete
  | Check of int * bool
  | CheckAll of bool
  | ChangeVisibility of string

let update model x =
  match x with
  | Add ->
      ( { model with
          uid= model.uid + 1
        ; field= ""
        ; entries=
            ( if model.field = "" then model.entries
              else
                IntMap.add model.uid
                  (newEntry model.field model.uid)
                  model.entries ) }
      , Cmd.none )
  | UpdateField field ->
      ({model with field}, Cmd.none)
  | EditingEntry (id, editing) ->
      let updateEntry t = if t.id = id then {t with editing} else t in
      ( { model with
          entries=
            ( if IntMap.mem id model.entries then
                IntMap.add id
                  (updateEntry (IntMap.find id model.entries))
                  model.entries
              else model.entries ) }
      , if editing then Cmds.focus ("todo-" ^ Belt.Int.toString id)
        else Cmd.none )
  | UpdateEntry (id, description) ->
      let updateEntry t = if t.id = id then {t with description} else t in
      ( { model with
          entries=
            ( if IntMap.mem id model.entries then
                IntMap.add id
                  (updateEntry (IntMap.find id model.entries))
                  model.entries
              else model.entries ) }
      , Cmd.none )
  | Delete id ->
      ({model with entries= IntMap.remove id model.entries}, Cmd.none)
  | DeleteComplete ->
      ( { model with
          entries=
            IntMap.filter
              (fun _id {completed; _} -> not completed)
              model.entries }
      , Cmd.none )
  | Check (id, completed) ->
      let updateEntry t = if t.id = id then {t with completed} else t in
      ( { model with
          entries=
            ( if IntMap.mem id model.entries then
                IntMap.add id
                  (updateEntry (IntMap.find id model.entries))
                  model.entries
              else model.entries ) }
      , Cmd.none )
  | CheckAll completed ->
      let updateEntry t = {t with completed} in
      ({model with entries= IntMap.map updateEntry model.entries}, Cmd.none)
  | ChangeVisibility visibility ->
      ({model with visibility}, Cmd.none)

let onEnter ?(key = "") msg =
  let tagger ev =
    match Webapi.Dom.KeyboardEvent.key (Obj.magic ev) with
    | "Enter" ->
        Some msg
    | _ ->
        None
  in
  onCB "keydown" ~key tagger

let viewEntry todo () =
  let key = Belt.Int.toString todo.id in
  let fullkey =
    key ^ string_of_bool todo.completed ^ string_of_bool todo.editing
  in
  li ~key:fullkey
    [ Html.Attributes.classList
        [("completed", todo.completed); ("editing", todo.editing)] ]
    [ div
        [Html.Attributes.class' "view"]
        [ input'
            [ Html.Attributes.class' "toggle"
            ; Html.Attributes.type' "checkbox"
            ; Html.Attributes.checked todo.completed
            ; onClick (Check (todo.id, not todo.completed)) ]
            []
        ; label
            [onDoubleClick (EditingEntry (todo.id, true))]
            [text todo.description]
        ; button [Html.Attributes.class' "destroy"; onClick (Delete todo.id)] []
        ]
    ; input'
        [ Html.Attributes.class' "edit"
        ; Html.Attributes.value todo.description
        ; Html.Attributes.name "title"
        ; Html.Attributes.id ("todo-" ^ Belt.Int.toString todo.id)
        ; onInput ~key (fun value -> UpdateEntry (todo.id, value))
        ; onBlur (EditingEntry (todo.id, false))
        ; onEnter ~key (EditingEntry (todo.id, false)) ]
        [] ]

let viewEntries visibility entries =
  let isVisible todo =
    match visibility with
    | "Completed" ->
        todo.completed
    | "Active" ->
        not todo.completed
    | _ ->
        true
  in
  let allCompleted =
    IntMap.for_all (fun _id {completed; _} -> completed) entries
  in
  let cssVisibility = if IntMap.is_empty entries then "hidden" else "visible" in
  section
    [ Html.Attributes.class' "main"
    ; Html.Attributes.style "visibility" cssVisibility ]
    [ input'
        [ Html.Attributes.class' "toggle-all"
        ; Html.Attributes.type' "checkbox"
        ; Html.Attributes.name "toggle"
        ; Html.Attributes.checked allCompleted
        ; onClick (CheckAll (not allCompleted)) ]
        []
    ; label [Html.Attributes.for' "toggle-all"] [text "Mark all as complete"]
    ; ul
        [Html.Attributes.class' "todo-list"]
        ( IntMap.bindings entries
        |> List.map (fun (_id, todo) ->
               if isVisible todo then
                 lazy1
                   ( Belt.Int.toString todo.id
                   ^ string_of_bool todo.completed
                   ^ string_of_bool todo.editing )
                   (viewEntry todo)
               else noNode ) ) ]

let viewInput task () =
  header ~key:task
    [Html.Attributes.class' "header"]
    [ h1 [] [text "todos"]
    ; input'
        [ Html.Attributes.class' "new-todo"
        ; Html.Attributes.placeholder "What needs to be done?"
        ; Html.Attributes.autofocus true
        ; Html.Attributes.value task
        ; Html.Attributes.name "newTodo"
        ; onInput (fun str -> UpdateField str)
        ; onEnter Add ]
        [] ]

let viewControlsCount entriesLeft =
  let item_ = if entriesLeft == 1 then " item" else " items" in
  let left = Belt.Int.toString entriesLeft in
  span ~key:left
    [Html.Attributes.class' "todo-count"]
    [strong [] [text left]; text (item_ ^ " left")]

let visibilitySwap uri visibility actualVisibility =
  li
    [onClick (ChangeVisibility visibility)]
    [ a
        [ Html.Attributes.href uri
        ; Html.Attributes.classList [("selected", visibility = actualVisibility)]
        ]
        [text visibility] ]

let viewControlsFilters visibility =
  ul
    [Html.Attributes.class' "filters"]
    [ visibilitySwap "#/" "All" visibility
    ; text " "
    ; visibilitySwap "#/active" "Active" visibility
    ; text " "
    ; visibilitySwap "#/completed" "Completed" visibility ]

let viewControlsClear entriesCompleted =
  button
    [ Html.Attributes.class' "clear-completed"
    ; Html.Attributes.hidden (entriesCompleted == 0)
    ; onClick DeleteComplete ]
    [text ("Clear completed (" ^ Belt.Int.toString entriesCompleted ^ ")")]

let viewControls visibility entries =
  let entriesCompleted =
    IntMap.fold
      (fun _id {completed; _} c -> if completed then c + 1 else c)
      entries 0
  in
  let len = IntMap.cardinal entries in
  let entriesLeft = len - entriesCompleted in
  footer
    [Html.Attributes.class' "footer"; Html.Attributes.hidden (len = 0)]
    [ viewControlsCount entriesLeft
    ; viewControlsFilters visibility
    ; viewControlsClear entriesCompleted ]

let view model =
  div
    [ Html.Attributes.class' "todomvc-wrapper"
    ; Html.Attributes.style "visibility" "hidden" ]
    [ section
        [Html.Attributes.class' "todoapp"]
        [ lazy1 model.field (viewInput model.field)
        ; viewEntries model.visibility model.entries
        ; viewControls model.visibility model.entries ] ]

let main =
  standardProgram {init; update; view; subscriptions= (fun _model -> Sub.none)}
