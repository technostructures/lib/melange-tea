open Tea
open App
open Html
open Html.Events
module Cmds = Tea_html_cmds

type nonrec entry =
  {description: string; completed: bool; editing: bool; id: int}

type nonrec model =
  {entries: entry list; field: string; uid: int; visibility: string}

let emptyModel = {entries= []; visibility= "All"; field= ""; uid= 0}

let newEntry desc id = {description= desc; completed= false; editing= false; id}

let init () = (emptyModel, Cmd.none)

type nonrec msg =
  | NoOp
  | UpdateField of string
  | EditingEntry of int * bool
  | UpdateEntry of int * string
  | Add
  | Delete of int
  | DeleteComplete
  | Check of int * bool
  | CheckAll of bool
  | ChangeVisibility of string

let update model x =
  match x with
  | NoOp ->
      (model, Cmd.none)
  | Add ->
      ( { model with
          uid= model.uid + 1
        ; field= ""
        ; entries=
            ( if model.field = "" then model.entries
              else model.entries @ [newEntry model.field model.uid] ) }
      , Cmd.none )
  | UpdateField field ->
      ({model with field}, Cmd.none)
  | EditingEntry (id, editing) ->
      let updateEntry t = if t.id = id then {t with editing} else t in
      ( {model with entries= List.map updateEntry model.entries}
      , if editing then Cmds.focus ("todo-" ^ string_of_int id) else Cmd.none )
  | UpdateEntry (id, description) ->
      let updateEntry t = if t.id = id then {t with description} else t in
      ({model with entries= List.map updateEntry model.entries}, Cmd.none)
  | Delete id ->
      ( {model with entries= List.filter (fun t -> t.id <> id) model.entries}
      , Cmd.none )
  | DeleteComplete ->
      ( { model with
          entries=
            List.filter (fun {completed; _} -> not completed) model.entries }
      , Cmd.none )
  | Check (id, completed) ->
      let updateEntry t = if t.id = id then {t with completed} else t in
      ({model with entries= List.map updateEntry model.entries}, Cmd.none)
  | CheckAll completed ->
      let updateEntry t = {t with completed} in
      ({model with entries= List.map updateEntry model.entries}, Cmd.none)
  | ChangeVisibility visibility ->
      ({model with visibility}, Cmd.none)

let onEnter ?(key = "") msg =
  let tagger ev =
    match Webapi.Dom.KeyboardEvent.key (Obj.magic ev) with
    | "Enter" ->
        Some msg
    | _ ->
        None
  in
  onCB "keydown" ~key tagger

let viewEntry todo =
  let key = string_of_int todo.id in
  li
    [ Html.Attributes.classList
        [("completed", todo.completed); ("editing", todo.editing)] ]
    [ div
        [Html.Attributes.class' "view"]
        [ input'
            [ Html.Attributes.class' "toggle"
            ; Html.Attributes.type' "checkbox"
            ; Html.Attributes.checked todo.completed
            ; onClick (Check (todo.id, not todo.completed)) ]
            []
        ; label
            [onDoubleClick (EditingEntry (todo.id, true))]
            [text todo.description]
        ; button [Html.Attributes.class' "destroy"; onClick (Delete todo.id)] []
        ]
    ; input'
        [ Html.Attributes.class' "edit"
        ; Html.Attributes.value todo.description
        ; Html.Attributes.name "title"
        ; Html.Attributes.id ("todo-" ^ string_of_int todo.id)
        ; onInput ~key (fun value -> UpdateEntry (todo.id, value))
        ; onBlur (EditingEntry (todo.id, false))
        ; onEnter ~key (EditingEntry (todo.id, false)) ]
        [] ]

let viewEntries visibility entries =
  let isVisible todo =
    match visibility with
    | "Completed" ->
        todo.completed
    | "Active" ->
        not todo.completed
    | _ ->
        true
  in
  let allCompleted = List.for_all (fun {completed; _} -> completed) entries in
  let cssVisibility = if [] = entries then "hidden" else "visible" in
  section
    [ Html.Attributes.class' "main"
    ; Html.Attributes.style "visibility" cssVisibility ]
    [ input'
        [ Html.Attributes.class' "toggle-all"
        ; Html.Attributes.type' "checkbox"
        ; Html.Attributes.name "toggle"
        ; Html.Attributes.checked allCompleted
        ; onClick (CheckAll (not allCompleted)) ]
        []
    ; label [Html.Attributes.for' "toggle-all"] [text "Mark all as complete"]
    ; ul
        [Html.Attributes.class' "todo-list"]
        (List.map viewEntry (List.filter isVisible entries)) ]

let viewInput task =
  header
    [Html.Attributes.class' "header"]
    [ h1 [] [text "todos"]
    ; input'
        [ Html.Attributes.class' "new-todo"
        ; Html.Attributes.placeholder "What needs to be done?"
        ; Html.Attributes.autofocus true
        ; Html.Attributes.value task
        ; Html.Attributes.name "newTodo"
        ; onInput (fun str -> UpdateField str)
        ; onEnter Add ]
        [] ]

let viewControlsCount entriesLeft =
  let item_ = if entriesLeft == 1 then " item" else " items" in
  span
    [Html.Attributes.class' "todo-count"]
    [strong [] [text (string_of_int entriesLeft)]; text (item_ ^ " left")]

let visibilitySwap uri visibility actualVisibility =
  li
    [onClick (ChangeVisibility visibility)]
    [ a
        [ Html.Attributes.href uri
        ; Html.Attributes.classList [("selected", visibility = actualVisibility)]
        ]
        [text visibility] ]

let viewControlsFilters visibility =
  ul
    [Html.Attributes.class' "filters"]
    [ visibilitySwap "#/" "All" visibility
    ; text " "
    ; visibilitySwap "#/active" "Active" visibility
    ; text " "
    ; visibilitySwap "#/completed" "Completed" visibility ]

let viewControlsClear entriesCompleted =
  button
    [ Html.Attributes.class' "clear-completed"
    ; Html.Attributes.hidden (entriesCompleted == 0)
    ; onClick DeleteComplete ]
    [text ("Clear completed (" ^ string_of_int entriesCompleted ^ ")")]

let viewControls visibility entries =
  let entriesCompleted =
    List.length (List.filter (fun {completed; _} -> completed) entries)
  in
  let entriesLeft = List.length entries - entriesCompleted in
  footer
    [ Html.Attributes.class' "footer"
    ; Html.Attributes.hidden (List.length entries = 0) ]
    [ viewControlsCount entriesLeft
    ; viewControlsFilters visibility
    ; viewControlsClear entriesCompleted ]

let view model =
  Js.log "view";
  div
    [Html.Attributes.class' "todomvc-wrapper"]
    [ section
        [Html.Attributes.class' "todoapp"]
        [ viewInput model.field
        ; viewEntries model.visibility model.entries
        ; viewControls model.visibility model.entries ] ]

let main =
  standardProgram {init; update; view; subscriptions= (fun _model -> Sub.none)}
