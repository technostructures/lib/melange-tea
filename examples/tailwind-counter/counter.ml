[%%mel.raw "import './app.css'"]

open Tea.App
open Tea.Html
open Tea_html.Events
open Tea_html.Attributes

type msg = Increment | Decrement | Reset 
type model = int

let update model x =
  match x with
  | Increment ->
      let () = Js.log "Incr" in
      model + 1
  | Decrement ->
      model - 1
  | Reset ->
      0


let view_button title msg =
  div
    [ class' "p-4" ]
    [ button 
       [class' "bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded"; onClick msg] 
       [text title]
    ]
      

let view model =
  div
    [class' "p-10 m-auto"]
    [ 
      h1
        [class' "text-center pb-10 text-lg font-bold"]
        [text "Melange TEA - Elm architecture for Melange"]
    ; h2
        [class' "text-center"]
        [text (Stdlib.string_of_int model)]
    ; div
        [class' "flex flex-horizontal p-10 m-auto justify-center"]
        [ view_button "Increment" Increment 
        ; view_button "Decrement" Decrement
        ; (if model <> 0 then view_button "Reset" Reset else noNode)
        ] 
    ] 

let node = 
     Webapi.Dom.document 
  |> Webapi.Dom.Document.getElementById "app" 
  |> Option.map Webapi.Dom.Element.asNode 

let _main = beginnerProgram {model= 4; update; view;} node () 


 
