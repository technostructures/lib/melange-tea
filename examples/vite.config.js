import { resolve } from 'path';
import { defineConfig, searchForWorkspaceRoot } from "vite";
import melangePlugin from "vite-plugin-melange";

export default defineConfig({
  base: "",
  plugins: [
    melangePlugin({
      duneDir: "..",
      buildCommand: "opam exec -- dune build @examples",
      watchCommand: "opam exec -- dune build --watch @examples",
    }),
  ],
  server: {
    fs: {
      allow: [
        searchForWorkspaceRoot(process.cwd()),
        '../_build'
      ],
    },
  },
  build: {
    rollupOptions: {
      input: {
        main: resolve(__dirname , 'index.html'),
        tests: resolve(__dirname , 'tests/index.html'),
        tailwindCounter: resolve(__dirname , 'tailwind-counter/index.html'),
      },
    },
  },
});
